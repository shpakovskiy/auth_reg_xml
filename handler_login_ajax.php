<?php
require "functions.php";

if(!empty($_POST['login']) && !empty($_POST['password'])) {


    $login = clean($_POST['login']);
    $password = clean($_POST['password']);




    $xml = simplexml_load_file("db.xml") or die("Error: Cannot create object");

    $content = file_get_contents('db.xml');
    $sxe = new SimpleXMLElement($content);

    try {


        foreach ($xml->children() as $user) {
            $login_counts=0;
            if ($user->login == $login) {
                $login_counts++;
                if (md5($password.$user->salt) != $user->password) {

                    throw new Exception("Error: Password confirmation does not match");
                }
                if (md5($password.$user->salt) == $user->password) {
                    session_start();
                    $_SESSION['auth'] = true;
                    $_SESSION['login'] = (string)$user->login;
                    setcookie('login', $user->login, time()+60*60*24*30);


                    header("Location:/");exit;
                    //echo "Hello $login !";
                }




            }


        }


        if($login_counts===0){throw new Exception("Error: Wrong login");}


    } catch (Exception $e) {
        $errors= $e->getMessage();
    }

    $result = array(
        'success'=>true,
        'login' => $login,
        'errors' => $errors,

    );


    echo json_encode($result);

}


<?php
require "functions.php";

if (!empty($_POST['login']) && !empty($_POST['password']) && !empty($_POST['confirm_password']) && !empty($_POST['email']) && !empty($_POST['name'])) {


    $login = clean($_POST['login']);
    $email = clean($_POST['email']);

    $name = clean($_POST['name']);
    $password = clean($_POST['password']);
    $confirm_password = clean($_POST['confirm_password']);

    $xml = simplexml_load_file("db.xml") or die("Error: Cannot create object");
    $content = file_get_contents('db.xml');
    $sxe = new SimpleXMLElement($content);

    try {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Error:Invalid email format");
        }

        foreach ($xml->children() as $user) {

            if ($user->login == $login) {
                throw new Exception("Error: Login already exists");
            }
            if ($email == $user->email) {
                throw new Exception("Error: Email already exists");
            }

        }

        if ($password != $confirm_password) {
            throw new Exception("Error: Password confirmation does not match");
        }
        $salt = generateSalt();
        $password = md5($password . $salt);


        $user = $sxe->addChild('user');
        $user->addChild('login', $login);
        $user->addChild('password', $password);
        $user->addChild('salt', $salt);
        $user->addChild('email', $email);
        $user->addChild('name', $name);

        $sxe->asXML('db.xml');


    } catch (Exception $e) {
        echo $e->getMessage();
    }


} ?>
<script type="text/javascript">
    window.location.href = '/';
</script>




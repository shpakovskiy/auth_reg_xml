<?php include 'header.php';?>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
<form action="handler_register.php" method="post" oninput='confirm_password.setCustomValidity(confirm_password.value != password.value ? "Passwords do not match." : "")'>


    <div class="form-group">
        <label for="login">Login</label>
        <input class="form-control" type="text" placeholder="Login" id="login" required="" name="login">
    </div>



    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" placeholder="Password" required="" name="password">
    </div>
    <div class="form-group">
        <label for="confirm_password">Confirm Password</label>
        <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password" required="" name="confirm_password">
    </div>

    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" id="email" placeholder="Enter email" required="" name="email">

    </div>

    <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" type="text" placeholder="name" id="name" required="" name="name">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>